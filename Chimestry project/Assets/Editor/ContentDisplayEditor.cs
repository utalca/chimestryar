﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ContentDisplay))]
[CanEditMultipleObjects]
public class ContentDisplayEditor : Editor {

    public void ShowArrayProperty(SerializedProperty list)
    {

        EditorGUI.indentLevel += 1;
        for (int i = 0; i < list.arraySize; i++)
        {
            int index = 100 / list.arraySize;

            EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i), new GUIContent((index * (i + 1)) + "%"));
        }
        EditorGUI.indentLevel -= 1;

        serializedObject.ApplyModifiedProperties();
    }

    public override void OnInspectorGUI()
    {
        ContentDisplay myTarget = (ContentDisplay)target;

        if (GUILayout.Button("+"))
        {
            myTarget.AddSlot();
        }

        if (serializedObject.FindProperty("states").arraySize > 0)
        {
            if (GUILayout.Button("-"))
            {
                myTarget.RemoveSlot();
            }
        }


        ShowArrayProperty(serializedObject.FindProperty("states"));
    }
}
