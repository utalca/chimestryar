﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Essentials;

[System.Serializable]
public class Compound {

    public enum StatestateOfMatter
    {
        LIQUID,
        SOLID,
        GASEOUS
    }

    public Element[] elements;
    public float celcius = 24;
    public RegisteredCompounds compound = RegisteredCompounds.H2O;
    public Color color;

    [Space]
    public float meltingPoint = 0;
    public float boilingPoint = 100;
    public float density = 1;

    private float _volume;
    private float _weight;

    public float Volume
    {
        get
        {
            return _volume;
        }

        set
        {
            _volume = value;
            _weight = _volume / density;
        }
    }

    public float Weight
    {
        get
        {
            return _weight;
        }

        set
        {
            _weight = value;
            _volume = _weight * density;
        }
    }

    public float Molarity
    {
        get
        {
            return Weight / ((float)MolecularWeight * Volume * 1000);
        }
    }

    public StatestateOfMatter State
    {
        get
        {
            if (celcius > boilingPoint)
            {
                return StatestateOfMatter.GASEOUS;
            }

            if (celcius < meltingPoint)
            {
                return StatestateOfMatter.SOLID;
            }

            return StatestateOfMatter.LIQUID;
        }
    }

    public string Name
    {
        get
        {
            string aux = string.Empty;

            foreach (Element e in elements)
            {
                aux += e.symbol.ToString();

                if (e.amount > 1)
                    aux += e.amount;
            }

            return aux;
        }
    }

    public double MolecularWeight
    {
        get
        {
            double aux = 0;
            foreach (Element e in elements)
            {
                aux += (e.Weight * e.amount);
            }

            return aux;
        }
    }

    public Compound(Element[] elements)
    {
        this.elements = elements;
    }

    public Compound(RegisteredCompounds compound)
    {
        Compound aux = Compound.CloneCompound(CompoundDataBase.GetCompound(compound));

        this.elements = aux.elements;
        this.color = aux.color;
        this.celcius = aux.celcius;
        this.compound = aux.compound;
        this.meltingPoint = aux.meltingPoint;
        this.boilingPoint = aux.boilingPoint;
        this.density = aux.density;
    }

    public Compound(RegisteredCompounds compound, float meltingPoint, float boilingPoint, float density, Color color)
    {
        this.elements = ElementsDatabase.GetElementsIn(compound);
        this.color = color;
        this.celcius = 24;
        this.compound = compound;
        this.meltingPoint = meltingPoint;
        this.boilingPoint = boilingPoint;
        this.density = density;
    }

    public static Compound[] CloneCompound(Compound[] c)
    {
        Compound[] aux = new Compound[c.Length];

        for (int i = 0; i < c.Length; i++)
        {
            aux[i] = new Compound(c[i].elements)
            {
                celcius = c[i].celcius,
                color = c[i].color,
                compound = c[i].compound,
                Volume = c[i].Volume,
                meltingPoint = c[i].meltingPoint,
                boilingPoint = c[i].boilingPoint,
                density = c[i].density
            };
        }

        return aux;
    }

    public static Compound CloneCompound(Compound c)
    {
        Compound aux = new Compound(c.elements)
        {
            celcius = c.celcius,
            color = c.color,
            compound = c.compound,
            Volume = c.Volume,
            meltingPoint = c.meltingPoint,
            boilingPoint = c.boilingPoint,
            density = c.density
        };

        return aux;
    }

    public static Compound[] CloneCompound(List<Compound> c)
    {
        Compound[] aux = new Compound[c.Count];

        for (int i = 0; i < c.Count; i++)
        {
            aux[i] = new Compound(c[i].elements)
            {
                celcius = c[i].celcius,
                color = c[i].color,
                compound = c[i].compound,
                Volume = c[i].Volume,
                meltingPoint = c[i].meltingPoint,
                boilingPoint = c[i].boilingPoint,
                density = c[i].density
            };
        }

        return aux;
    }

    public static Compound FindCompound(List<Compound> list, RegisteredCompounds target)
    {
        foreach (Compound c in list)
        {
            if (c.compound.Equals(target))
                return c;
        }

        return null;
    }

    public static Compound FindCompound(List<Compound> list, Compound target)
    {
        foreach (Compound c in list)
        {
            if (c.compound.Equals(target.compound))
                return c;
        }

        return null;
    }

    public static bool CompoundExists(List<Compound> list, Compound target)
    {
        foreach (Compound c in list)
        {
            if (c.compound.Equals(target.compound))
                return true;
        }

        return false;
    }

    public static bool CompoundExists(List<Compound> list, RegisteredCompounds target)
    {
        foreach (Compound c in list)
        {
            if (c.compound.Equals(target))
                return true;
        }

        return false;
    }

    public static bool CompoundExists(Compound[] list, RegisteredCompounds target)
    {
        foreach (Compound c in list)
        {
            if (c.compound.Equals(target))
                return true;
        }

        return false;
    }

    public static float GetTotalVolume(Compound[] compounds)
    {
        float totalVolume = 0;

        foreach (Compound c in compounds)
        {
            totalVolume += c.Volume;
        }

        return totalVolume;
    }

    public static Color GetColor(Compound[] compounds)
    {
        Color result = Color.clear;

        float totalVolume = GetTotalVolume(compounds);

        for (int i = 0; i < compounds.Length; i++)
        {
            result += (compounds[i].color * (compounds[i].Volume / totalVolume)); 
        }

        result /= compounds.Length;

        return result;
    }

    public static bool CanReact(Compound[] compounds)
    {
        for (int i = 0; i < compounds.Length; i++)
        {
            for (int e = compounds.Length - 1; e > i; e--)
            {
                if (ReactionRecipes.Singleton.CanReact(compounds[i].compound, compounds[e].compound))
                {
                    return true;
                }
            }
        }

        return false;
    }

    public static List<Compound> GetReaction(Compound[] compounds)
    {
        for (int i = 0; i < compounds.Length; i++)
        {
            for (int e = compounds.Length - 1; e > i; e--)
            {
                if (ReactionRecipes.Singleton.CanReact(compounds[i].compound, compounds[e].compound))
                {
                    return ReactionRecipes.Singleton.GetResult(compounds[i], compounds[e]);
                }
            }
        }

        Debug.LogError("[ERROR]: Reaction not found!");
        return null;
    }

    public static float GetVolume(Compound[] compounds)
    {
        float aux = 0;

        foreach (Compound c in compounds)
        {
            aux += c.Volume;
        }

        return aux;
    }
}

public enum RegisteredCompounds
{
    HCL,
    H2O,
    CO2,
    NACL,
    NAHCO3,
    NAOH
}

public class CompoundDataBase
{
    public static List<Compound> compounds = new List<Compound>();

    public static Compound GetCompound(RegisteredCompounds compound)
    {
        Compound aux = null;

        if (compounds.Count.Equals(0) || compounds == null)
        {
            PopulateList();
        }

        foreach (Compound c in compounds)
        {
            if (c.compound == compound)
            {
                aux = Compound.CloneCompound(c);
                break;
            }
        }

        return aux;
    }

    private static void PopulateList()
    {
        compounds.Add(new Compound(RegisteredCompounds.HCL, -26, 48, 1.189f, new Color(1, 1, 1, 0.7f)));
        compounds.Add(new Compound(RegisteredCompounds.H2O, 0, 100, 1, new Color(1,1,1,0.7f)));
        compounds.Add(new Compound(RegisteredCompounds.CO2, -78, -57, 773, new Color(1, 1, 1, 0.4f)));
        compounds.Add(new Compound(RegisteredCompounds.NACL, 801, 1465, 2.165f, Color.white));
        compounds.Add(new Compound(RegisteredCompounds.NAHCO3, 50, 270, 2.2f, Color.white));
        compounds.Add(new Compound(RegisteredCompounds.NAOH, -318, 1388, 2.13f, Color.white));
    }
}