﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Merge : ActionCard {

    enum ValidationExceptions
    {
        INVALID_RECIPE,
        SUCCESS
    }

    public GameObject[] errorIcons;
    public GameObject[] resultPrefabs;

    public GameObject actionIcon;

    private Container _container_Left;
    private Container _container_Right;
    private BlankCard _blanckCard_Down;


    public override void TurnOff()
    {
        base.TurnOff();
        Cancel();
    }

    public override void Interact()
    {
        try
        {
            _container_Left = _input_L.GetComponent<Instancer>().instance.GetComponent<Container>();
        }
        catch { }

        try
        {
            _container_Right = _input_R.GetComponent<Instancer>().instance.GetComponent<Container>();
        }
        catch { }

        try
        {
            _blanckCard_Down = _input_D.GetComponent<BlankCard>();
        }
        catch { }

        if (_container_Left == null || _container_Right == null || _blanckCard_Down == null)
            return;

        if (_blanckCard_Down.IsFilled)
            return;

        switch (ValidateRecipe(_container_Left, _container_Right))
        {
            case ValidationExceptions.SUCCESS:
                StartCoroutine(Combine(_container_Left, _container_Right));
                return;
            case ValidationExceptions.INVALID_RECIPE:
                ShowErrorIcon((int)ValidationExceptions.INVALID_RECIPE);
                break;
        }

        isInteracting = false;
    }

    private void ShowErrorIcon(int i)
    {
        errorIcons[i].SetActive(true);
    }

    private void ShowActionIcon()
    {
        actionIcon.SetActive(true);
    }

    public override void DisactiveIcons()
    {
        foreach (GameObject go in errorIcons)
        {
            go.SetActive(false);
        }

        actionIcon.SetActive(false);
    }

    private void Cancel()
    {
        DisactiveIcons();
    }

    private IEnumerator Combine(Container input, Container output)
    {
        DisactiveIcons();

        ShowActionIcon();

        yield return new WaitForSeconds(3);

        if (_container_Left != null && _container_Right != null && _blanckCard_Down != null)
        {
            AssignBlankCard();

            _container_Left.compound.Clear();
            _container_Right.compound.Clear();
        }

        DisactiveIcons();

        isInteracting = false;
    }

    private void AssignBlankCard()
    {
        object[] data = new object[2];
        data[0] = _container_Left;
        data[1] = _container_Right;

        switch (MergeRecipes.Singleton.GetResult(_container_Left.type, _container_Right.type))
        {
            case MergeResults.FLASK_BALLOON:
                _blanckCard_Down.AssignCard(resultPrefabs[(int)MergeResults.FLASK_BALLOON], data);
                break;
            case MergeResults.TITULATION:
                _blanckCard_Down.AssignCard(resultPrefabs[(int)MergeResults.TITULATION], data);
                break;
            case MergeResults.BOILING_STATION:
                _blanckCard_Down.AssignCard(resultPrefabs[(int)MergeResults.BOILING_STATION], data);
                break;
        }
    }

    private ValidationExceptions ValidateRecipe(Container input, Container output)
    {
        if (_container_Left == null || _container_Right == null)
            return ValidationExceptions.INVALID_RECIPE;

        if (MergeRecipes.Singleton.GetResult(_container_Left.type, _container_Right.type).Equals(MergeResults.NONE))
        {
            return ValidationExceptions.INVALID_RECIPE;
        }

        return ValidationExceptions.SUCCESS;
    }
}
