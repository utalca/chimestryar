﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Chemical : Container {

    public bool CompoundExists()
    {
        return true;
    }

    public override void Add(Compound[] compound)
    {
        return;
    }

    public override Compound[] Substract(float delta)
    {
        float proporcionalDelta = delta / compound.Count;

        Compound[] aux = Compound.CloneCompound(compound);

        for (int i = 0; i < compound.Count; i++)
        {
            aux[i].Volume = proporcionalDelta;
        }

        return aux;
    }
}

[System.Serializable]
public class Element
{
    public Symbol symbol;
    public int amount = 1;

    public Family Family
    {
        get
        {
            if (_fromPool == null)
                _fromPool = ElementsDatabase.GetElement(symbol);

            return _fromPool._family;
        }
    }

    public int AtomicNum
    {
        get
        {
            if (_fromPool == null)
                _fromPool = ElementsDatabase.GetElement(symbol);

            return _fromPool._atomicNum;
        }
    }

    public float Weight
    {
        get
        {
            if (_fromPool == null)
                _fromPool = ElementsDatabase.GetElement(symbol);

            return _fromPool._weight;
        }
    }

    private Element _fromPool;
    private Family _family;
    private int _atomicNum;
    private float _weight;

    public Element(Symbol sym, Family family, int atomicNum, float weight)
    {
        this.symbol = sym;
        this._family = family;
        this._atomicNum = atomicNum;
        this._weight = weight;
    }

    public Element(Symbol sym, int amount)
    {
        this.symbol = sym;
        this.amount = amount;
    }
}

[System.Serializable]
public static class ElementsDatabase {

    public static List<Element> elements = new List<Element>();

    public static Element GetElement(Symbol symbol)
    {
        Element aux = null;

        if (elements.Count.Equals(0))
        {
            PopulateList();
        }

        foreach (Element e in elements)
        {
            if (e.symbol == symbol)
            {
                aux = e;
                break;
            }
        }

        return aux;
    }

    public static Element[] GetElementsIn(RegisteredCompounds compound)
    {
        Element[] aux;

        switch (compound)
        {
            case RegisteredCompounds.HCL:
                aux = new Element[2];
                aux[0] = new Element(Symbol.H, 1);
                aux[1] = new Element(Symbol.Cl, 1);
                return aux;
            case RegisteredCompounds.H2O:
                aux = new Element[2];
                aux[0] = new Element(Symbol.H, 2);
                aux[1] = new Element(Symbol.O, 1);
                return aux;
            case RegisteredCompounds.CO2:
                aux = new Element[2];
                aux[0] = new Element(Symbol.C, 1);
                aux[1] = new Element(Symbol.O, 2);
                return aux;
            case RegisteredCompounds.NACL:
                aux = new Element[2];
                aux[0] = new Element(Symbol.Na, 1);
                aux[1] = new Element(Symbol.Cl, 1);
                return aux;
            case RegisteredCompounds.NAHCO3:
                aux = new Element[3];
                aux[0] = new Element(Symbol.Na, 1);
                aux[1] = new Element(Symbol.H, 1);
                aux[2] = new Element(Symbol.Co, 3);
                return aux;
        }

        return null;
    }

    private static void PopulateList()
    {
        elements.Add(new Element(Symbol.H, Family.NoMetal, 1, 1.008f));
        elements.Add(new Element(Symbol.He, Family.NoMetal, 2, 4.0026f));

        elements.Add(new Element(Symbol.Li, Family.Metal, 3, 6.94f));
        elements.Add(new Element(Symbol.Be, Family.Metal, 4, 9.0122f));
        elements.Add(new Element(Symbol.B, Family.Metaloid, 5, 10.81f));
        elements.Add(new Element(Symbol.C, Family.NoMetal, 6, 12.011f));
        elements.Add(new Element(Symbol.N, Family.NoMetal, 7, 14.007f));
        elements.Add(new Element(Symbol.O, Family.NoMetal, 8, 15.999f));
        elements.Add(new Element(Symbol.F, Family.NoMetal, 9, 18.998f));
        elements.Add(new Element(Symbol.Ne, Family.NoMetal, 10, 20.180f));

        elements.Add(new Element(Symbol.Na, Family.Metal, 11, 22.990f));
        elements.Add(new Element(Symbol.Mg, Family.Metal, 12, 24.305f));
        elements.Add(new Element(Symbol.Al, Family.Metal, 13, 26.982f));
        elements.Add(new Element(Symbol.Si, Family.Metaloid, 14, 28.085f));
        elements.Add(new Element(Symbol.P, Family.NoMetal, 15, 30.974f));
        elements.Add(new Element(Symbol.S, Family.NoMetal, 16, 32.06f));
        elements.Add(new Element(Symbol.Cl, Family.NoMetal, 17, 35.45f));
        elements.Add(new Element(Symbol.Ar, Family.NoMetal, 18, 39.948f));

        elements.Add(new Element(Symbol.K, Family.Metal, 19, 39.098f));
        elements.Add(new Element(Symbol.Ca, Family.Metal, 20, 40.078f));
        elements.Add(new Element(Symbol.Sc, Family.Metal, 21, 44.956f));
        elements.Add(new Element(Symbol.Ti, Family.Metal, 22, 47.867f));
        elements.Add(new Element(Symbol.V, Family.NoMetal, 23, 50.942f));
        elements.Add(new Element(Symbol.Cr, Family.Metal, 24, 51.996f));
        elements.Add(new Element(Symbol.Mn, Family.Metal, 25, 54.938f));
        elements.Add(new Element(Symbol.Fe, Family.Metal, 26, 55.845f));
        elements.Add(new Element(Symbol.Co, Family.Metal, 27, 58.933f));
        elements.Add(new Element(Symbol.Ni, Family.Metal, 28, 58.693f));
        elements.Add(new Element(Symbol.Cu, Family.Metal, 29, 63.546f));
        elements.Add(new Element(Symbol.Zn, Family.Metal, 30, 65.38f));
        elements.Add(new Element(Symbol.Ga, Family.Metal, 31, 69.723f));
        elements.Add(new Element(Symbol.Ge, Family.Metaloid, 32, 72.630f));
        elements.Add(new Element(Symbol.As, Family.Metaloid, 33, 74.922f));
        elements.Add(new Element(Symbol.Se, Family.NoMetal, 34, 78.971f));
        elements.Add(new Element(Symbol.Br, Family.NoMetal, 35, 79.904f));
        elements.Add(new Element(Symbol.Kr, Family.NoMetal, 36, 83.798f));

        elements.Add(new Element(Symbol.Rb, Family.Metal, 37, 85.468f));
        elements.Add(new Element(Symbol.Sr, Family.Metal, 38, 87.62f));
        elements.Add(new Element(Symbol.Y, Family.Metal, 39, 88.906f));
        elements.Add(new Element(Symbol.Zr, Family.Metal, 40, 91.224f));
        elements.Add(new Element(Symbol.Nb, Family.Metal, 41, 92.906f));
        elements.Add(new Element(Symbol.Mo, Family.Metal, 42, 95.95f));
        elements.Add(new Element(Symbol.Tc, Family.Metal, 43, 98f));
        elements.Add(new Element(Symbol.Ru, Family.Metal, 44, 101.07f));
        elements.Add(new Element(Symbol.Rh, Family.Metal, 45, 102.91f));
        elements.Add(new Element(Symbol.Pd, Family.Metal, 46, 106.42f));
        elements.Add(new Element(Symbol.Ag, Family.Metal, 47, 107.87f));
        elements.Add(new Element(Symbol.Cd, Family.Metal, 48, 112.41f));
        elements.Add(new Element(Symbol.In, Family.Metal, 49, 114.82f));
        elements.Add(new Element(Symbol.Sn, Family.Metal, 50, 118.71f));
        elements.Add(new Element(Symbol.Sb, Family.Metaloid, 51, 121.76f));
        elements.Add(new Element(Symbol.Te, Family.Metaloid, 52, 127.60f));
        elements.Add(new Element(Symbol.I, Family.NoMetal, 53, 126.90f));
        elements.Add(new Element(Symbol.Xe, Family.NoMetal, 54, 131.29f));

        elements.Add(new Element(Symbol.Cs, Family.Metal, 55, 132.91f));
        elements.Add(new Element(Symbol.Ba, Family.Metal, 56, 137.33f));
        elements.Add(new Element(Symbol.Hf, Family.Metal, 72, 178.49f));
        elements.Add(new Element(Symbol.Ta, Family.Metal, 73, 180.95f));
        elements.Add(new Element(Symbol.W, Family.Metal, 74, 183.84f));
        elements.Add(new Element(Symbol.Re, Family.Metal, 75, 186.21f));
        elements.Add(new Element(Symbol.Os, Family.Metal, 76, 190.23f));
        elements.Add(new Element(Symbol.Ir, Family.Metal, 77, 192.22f));
        elements.Add(new Element(Symbol.Pt, Family.Metal, 78, 195.08f));
        elements.Add(new Element(Symbol.Au, Family.Metal, 79, 196.97f));
        elements.Add(new Element(Symbol.Hg, Family.Metal, 80, 200.59f));
        elements.Add(new Element(Symbol.Tl, Family.Metal, 81, 204.38f));
        elements.Add(new Element(Symbol.Pb, Family.Metal, 82, 207.2f));
        elements.Add(new Element(Symbol.Bi, Family.Metal, 83, 208.98f));
        elements.Add(new Element(Symbol.Po, Family.Metal, 84, 209f));
        elements.Add(new Element(Symbol.At, Family.Metaloid, 85, 210f));
        elements.Add(new Element(Symbol.Rn, Family.NoMetal, 86, 222f));

        elements.Add(new Element(Symbol.Fr, Family.Metal, 87, 223f));
        elements.Add(new Element(Symbol.Ra, Family.Metal, 88, 226f));

    }
}

[System.Serializable]
public enum Family
{
    Metal,
    Metaloid,
    NoMetal
}

[System.Serializable]
public enum Symbol
{
    H,
    He,
    Li,
    Be,
    B,
    C,
    N,
    O,
    F,
    Ne,
    Na,
    Mg,
    Al,
    Si,
    P,
    S,
    Cl,
    Ar,
    K,
    Ca,
    Sc,
    Ti,
    V,
    Cr,
    Mn,
    Fe,
    Co,
    Ni,
    Cu,
    Zn,
    Ga,
    Ge,
    As,
    Se,
    Br,
    Kr,
    Rb,
    Sr,
    Y,
    Zr,
    Nb,
    Mo,
    Tc,
    Ru,
    Rh,
    Pd,
    Ag,
    Cd,
    In,
    Sn,
    Sb,
    Te,
    I,
    Xe,
    Cs,
    Ba,
    Hf,
    Ta,
    W,
    Re,
    Os,
    Ir,
    Pt,
    Au,
    Hg,
    Tl,
    Pb,
    Bi,
    Po,
    At,
    Rn,
    Fr,
    Ra
}

