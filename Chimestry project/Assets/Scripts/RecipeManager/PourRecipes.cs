﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PourRecipes{

    private static PourRecipes _singleton;

    private PourResults[,] _combinations;

    public static PourRecipes Singleton
    {
        get
        {
            if (_singleton == null)
                _singleton = new PourRecipes();

            return _singleton;
        }
    }

    public PourResults[,] Combinations
    {
        get
        {
            if (_combinations == null)
                LoadCombinations();

            return _combinations;
        }
    }

    public PourResults GetResult(ContainersTypes input, ContainersTypes output)
    {
        return Combinations[(int)input, (int)output];
    }

    private void LoadCombinations()
    {
        _combinations = new PourResults[System.Enum.GetValues(typeof(ContainersTypes)).Length, System.Enum.GetValues(typeof(ContainersTypes)).Length];

        for (int x = 0; x < System.Enum.GetValues(typeof(ContainersTypes)).Length; x++)
        {
            for (int y = 0; y < System.Enum.GetValues(typeof(ContainersTypes)).Length; y++)
            {
                _combinations[x, y] = PourResults.NONE;
            }
        }

        //Spoon
        _combinations[6, 4] = PourResults.SPOON_DRAIN;
        _combinations[4, 0] = PourResults.SPOON_FILL;
        _combinations[0, 4] = PourResults.SPOON_DRAIN;
        _combinations[4, 5] = PourResults.SPOON_FILL;
        _combinations[5, 4] = PourResults.SPOON_DRAIN;
        _combinations[4, 1] = PourResults.SPOON_FILL;
        _combinations[1, 4] = PourResults.SPOON_DRAIN;
        _combinations[4, 2] = PourResults.SPOON_FILL;
        _combinations[2, 4] = PourResults.SPOON_DRAIN;

        //Pippette
        _combinations[6, 3] = PourResults.PIPPETTE_DRAIN;
        _combinations[3, 0] = PourResults.PIPPETTE_FILL;
        _combinations[0, 3] = PourResults.PIPPETTE_DRAIN;
        _combinations[3, 5] = PourResults.PIPPETTE_FILL;
        _combinations[5, 3] = PourResults.PIPPETTE_DRAIN;
        _combinations[3, 1] = PourResults.PIPPETTE_FILL;
        _combinations[1, 3] = PourResults.PIPPETTE_DRAIN;
        _combinations[3, 2] = PourResults.PIPPETTE_FILL;
        _combinations[2, 3] = PourResults.PIPPETTE_DRAIN;
    }

}

public enum PourResults
{
    NONE = -1,
    DEFAULT = 0,
    SPOON_DRAIN = 1,
    SPOON_FILL = 2,
    PIPPETTE_DRAIN = 3,
    PIPPETTE_FILL = 4,
}
