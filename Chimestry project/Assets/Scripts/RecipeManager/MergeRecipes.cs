﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MergeRecipes{

    private static MergeRecipes _singleton;

    private MergeResults[,] _combinations;

    public static MergeRecipes Singleton
    {
        get
        {
            if (_singleton == null)
                _singleton = new MergeRecipes();

            return _singleton;
        }
    }

    public MergeResults[,] Combinations
    {
        get
        {
            if (_combinations == null)
                LoadCombinations();

            return _combinations;
        }
    }

    public MergeResults GetResult(ContainersTypes input, ContainersTypes output)
    {
        return Combinations[(int)input, (int)output];
    }

    private void LoadCombinations()
    {
        _combinations = new MergeResults[System.Enum.GetValues(typeof(ContainersTypes)).Length, System.Enum.GetValues(typeof(ContainersTypes)).Length];

        for (int x = 0; x < System.Enum.GetValues(typeof(ContainersTypes)).Length; x++)
        {
            for (int y = 0; y < System.Enum.GetValues(typeof(ContainersTypes)).Length; y++)
            {
                _combinations[x, y] = MergeResults.NONE;
            }
        }

        _combinations[1, 5] = MergeResults.FLASK_BALLOON;
        _combinations[5, 1] = MergeResults.FLASK_BALLOON;

        _combinations[3, 2] = MergeResults.TITULATION;
        _combinations[2, 3] = MergeResults.TITULATION;

        _combinations[2, 9] = MergeResults.BOILING_STATION;
        _combinations[9, 2] = MergeResults.BOILING_STATION;
    }
}

public enum MergeResults
{
    NONE = -1,
    FLASK_BALLOON = 0,
    TITULATION = 1,
    BOILING_STATION = 2
}
