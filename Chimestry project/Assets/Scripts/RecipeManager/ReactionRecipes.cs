﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ReactionRecipes : MonoBehaviour
{
    private static ReactionRecipes _singleton;

    private RegisterReactions[,] _combinations;

    public static ReactionRecipes Singleton
    {
        get
        {
            if (_singleton == null)
                _singleton = new ReactionRecipes();

            return _singleton;
        }
    }

    public RegisterReactions[,] Combinations
    {
        get
        {
            if (_combinations == null)
                LoadCombinations();

            return _combinations;
        }
    }

    public bool CanReact(RegisteredCompounds reactiveA, RegisteredCompounds reactiveB)
    {
        if (Combinations[(int)reactiveA, (int)reactiveB].Equals(RegisterReactions.NONE))
        {
            return false;
        }

        return true;
    }

    public List<Compound> GetResult(Compound reactiveA, Compound reactiveB)
    {
        switch (Combinations[(int)reactiveA.compound, (int)reactiveB.compound])
        {
            case RegisterReactions.HCl_NaHCo3:
                return ReactionDatabase.HCl_NaHCo3(reactiveA, reactiveB);
            case RegisterReactions.NONE:
                return null;
        }

        return null;
    }

    private void LoadCombinations()
    {
        _combinations = new RegisterReactions[System.Enum.GetValues(typeof(RegisteredCompounds)).Length, System.Enum.GetValues(typeof(RegisteredCompounds)).Length];

        for (int x = 0; x < System.Enum.GetValues(typeof(RegisteredCompounds)).Length; x++)
        {
            for (int y = 0; y < System.Enum.GetValues(typeof(RegisteredCompounds)).Length; y++)
            {
                _combinations[x, y] = RegisterReactions.NONE;
            }
        }
        _combinations[0, 4] = RegisterReactions.HCl_NaHCo3;
        _combinations[4, 0] = RegisterReactions.HCl_NaHCo3;
    }
}

public enum RegisterReactions
{
    HCl_NaHCo3,
    NONE
}

public static class ReactionDatabase
{
    public static List<Compound> HCl_NaHCo3(Compound a, Compound b)
    {
        List<Compound> aux = new List<Compound>();

        Compound hcl = null;
        Compound nahco3 = null;

        if (a.compound.Equals(RegisteredCompounds.HCL))
        {
            hcl = a;
            nahco3 = b;
        }
        else
        {
            hcl = b;
            nahco3 = a;
        }

        Compound nacl = new Compound(RegisteredCompounds.NACL);
        Compound h2o = new Compound(RegisteredCompounds.H2O);
        Compound co2 = new Compound(RegisteredCompounds.CO2);

        float totalWeight = hcl.Weight + nahco3.Weight;

        float nacl_weight = (float)nacl.MolecularWeight;

        float h2o_weight = (float)h2o.MolecularWeight;
        float co2_weight = (float)co2.MolecularWeight;
        float total_molecularWeight = nacl_weight + h2o_weight + co2_weight;

        nacl.Volume = totalWeight * (nacl_weight / total_molecularWeight);
        h2o.Volume = totalWeight * (h2o_weight / total_molecularWeight);
        co2.Volume = totalWeight * (co2_weight / total_molecularWeight);

        aux.Add(nacl);
        aux.Add(h2o);
        aux.Add(co2);

        return aux;
    }
}