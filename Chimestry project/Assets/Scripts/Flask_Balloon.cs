﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flask_Balloon : Container {

    public GameObject canvas;

    public override void Init(object[] data)
    {
        Container prev_Input = (Container)data[0];
        Container prev_Output = (Container)data[1];

        compound = new List<Compound>();

        capacity = prev_Input.capacity + prev_Output.capacity;

        compound.AddRange(prev_Input.compound);
        compound.AddRange(prev_Output.compound);

        prev_Input.DisplayVolume();
        prev_Output.DisplayVolume();
        DisplayVolume();
    }

    public override void Interact()
    {
        base.Interact();

        FindGas();
    }

    private void FindGas()
    {
        foreach (Compound c in compound)
        {
            if (c.State.Equals(Compound.StatestateOfMatter.GASEOUS))
            {
                GetComponentInChildren<Animator>().SetTrigger("Use");
                canvas.SetActive(true);
                break;
            }
        }
    }
}
