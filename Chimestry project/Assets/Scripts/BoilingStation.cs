﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoilingStation : Container {

    public GameObject particle;
    public bool isEnable;
    public Renderer glass;
    public GameObject steam;

    private Color originalColor;

    private void OnEnable()
    {
        particle.SetActive(isEnable);
    }

    public override void Init(object[] data)
    {
        Container prev_Input = (Container)data[0];
        Container prev_Output = (Container)data[1];

        if (prev_Input.type.Equals(ContainersTypes.DESTILATION))
            capacity = prev_Input.capacity;
        else
            capacity = prev_Output.capacity;

        compound = new List<Compound>();

        compound.AddRange(prev_Input.compound);
        compound.AddRange(prev_Output.compound);

        prev_Input.DisplayVolume();
        prev_Output.DisplayVolume();
        DisplayVolume();

        originalColor = glass.material.color;
    }

    public void Update()
    {
        if (isEnable)
        {
            Boiling();
        }
        else
        {
            Cooling();
        }
    }

    public override void Interact()
    {
        base.Interact();

        isEnable = !isEnable;

        particle.SetActive(isEnable);
    }

    private void Boiling()
    {
        Color steamColor = Color.clear;
        List<Color> compoundSteam = new List<Color>();

        foreach (Compound c in compound)
        {
            c.celcius += (Time.deltaTime * 2);
            c.celcius = Mathf.Clamp(c.celcius, 24, 500);

            if(!CompatibleMatter(c.State))
            {
                c.Volume -= (Time.deltaTime / 10);
                compoundSteam.Add(c.color);

                steam.SetActive(true);

                if(c.Volume <= 0)
                {
                    steam.SetActive(false);
                    compound.Remove(c);
                    break;
                }

                DisplayVolume();
            }
        }

        foreach (Color c in compoundSteam)
        {
            steamColor += c;
        }

        steamColor /= compoundSteam.Count;

        ParticleSystem.MainModule settings = steam.GetComponent<ParticleSystem>().main;
        settings.startColor = new ParticleSystem.MinMaxGradient(steamColor);

        glass.material.color = Color.Lerp(glass.material.color, Color.red, Time.deltaTime / 30);
    }

    private void Cooling()
    {
        Color steamColor = Color.clear;
        List<Color> compoundSteam = new List<Color>();

        foreach (Compound c in compound)
        {
            print("Compuesto: " + c.compound + " Volumen: " + c.Volume);
            c.celcius -= (Time.deltaTime / 4);
            c.celcius = Mathf.Clamp(c.celcius, 24, 500);

            if (!CompatibleMatter(c.State))
            {
                c.Volume -= (Time.deltaTime / 10);
                compoundSteam.Add(c.color);

                steam.SetActive(true);

                if (c.Volume <= 0)
                {
                    steam.SetActive(false);
                    compound.Remove(c);
                    break;
                }

                DisplayVolume();
            }
        }

        glass.material.color = Color.Lerp(glass.material.color, originalColor, Time.deltaTime / 50);

        if (Temperature < 30)
        {
            steam.SetActive(false);
        }
    }

}
