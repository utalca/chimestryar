﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Upgrader : ActionCard {

    public Text error_t;

    public override void TurnOff()
    {
        base.TurnOff();
        Cancel();
    }

    public override void Interact()
    {
        Container o = _input_R.GetComponent<Instancer>().instance.GetComponent<Container>();

        if (o == null)
        {
            o = _input_L.GetComponent<Instancer>().instance.GetComponent<Container>();
        }
        {
            o = _input_R.GetComponent<Instancer>().instance.GetComponent<Container>();
        }

        if (o != null)
        {
            if (o.Volume > 0)
            {
                SendError();
                return;
            }

            if (o.Volume == -1)
            {
                SendError();
                return;
            }

            DisactiveError();

            switch(o.Size)
            {
                case Size.SMALLER:
                    o.Size = Size.SMALL;
                    break;
                case Size.SMALL:
                    o.Size = Size.MEDIUM;
                    break;
                case Size.MEDIUM:
                    o.Size = Size.BIG;
                    break;
                case Size.BIG:
                    o.Size = Size.BIGGER;
                    break;
                case Size.BIGGER:
                    o.Size = Size.SMALLER;
                    break;
            }
        }
    }

    private void SendError()
    {
        ActiveError();
        error_t.text = "El contenedor debe estar completamente vacio \n o no se puede cambiar su capacidad.";

        DisactiveError();
    }

    private void ActiveError()
    {
        Color r = error_t.color;
        r.a = 1;
        error_t.color = r;
    }

    private void DisactiveError()
    {
        Color r = error_t.color;
        r.a = 0;
        error_t.color = r;
    }

    private void Cancel()
    {
        DisactiveError();
    }
}
