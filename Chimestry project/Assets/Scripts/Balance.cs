﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Balance : InfoCard {

    public Text percentage;
    public Image bar_background;
    public Image bar_amount;

    public override void DisplayInfo()
    {
        try
        {
            Container[] c = card.GetComponentsInChildren<Container>();

            float volAux = 0;
            float capAux = 0;

            foreach (Container con in c)
            {
                volAux += con.Volume;
                capAux += con.capacity;
            }

            StartCoroutine(UpdateBar(volAux, capAux));

            text.text = volAux.ToString("F2") + " / " + capAux + " ";
        }
        catch
        {
            text.text = "No es posible pesar este objeto.";
            text.color = Color.red;
        }
    }
    
    private IEnumerator UpdateBar(float volume, float capacity)
    {
        float aux = 0.1f;

        float final = volume / capacity;

        do
        {
            percentage.text = ((aux / capacity) * 100).ToString("F2") + " %";

            bar_amount.fillAmount = aux;

            aux += (((aux / capacity)) / 1) * Time.deltaTime;

            yield return new WaitForSeconds(Time.deltaTime);
        }
        while (aux < final);

        percentage.text = (final * 100).ToString("F2") + " %";
        bar_amount.fillAmount = final;
    }

    public override void TurnOff()
    {
        base.TurnOff();
        DisactiveStatus();
    }

    public override void DisactiveStatus()
    {
        Color info = text.color;
        info.a = 0;
        text.color = info;
        Color perc = percentage.color;
        perc.a = 0;
        percentage.color = perc;
        Color bar_back = bar_background.color;
        bar_back.a = 0;
        bar_background.color = bar_back;
        Color bar_value = bar_amount.color;
        bar_value.a = 0;
        bar_amount.color = bar_value;
    }

    public override void ActiveStatus()
    {
        Color info = text.color;
        info.a = 1;
        text.color = info;
        Color perc = percentage.color;
        perc.a = 1;
        percentage.color = perc;
        Color bar_back = bar_background.color;
        bar_back.a = 1;
        bar_background.color = bar_back;
        Color bar_value = bar_amount.color;
        bar_value.a = 1;
        bar_amount.color = bar_value;
    }
}
