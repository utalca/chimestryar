﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ActionCard : MonoBehaviour {

    public GameObject particle_L;
    public GameObject particle_D;
    public GameObject particle_R;

    [HideInInspector] public GameObject _input_L;
    [HideInInspector] public GameObject _input_R;
    [HideInInspector] public GameObject _input_D;

    public bool isInteracting;

    protected virtual void Update()
    {
        if (_input_L != null)
        {
            try
            {
                particle_L.SetActive(true);
            }
            catch { }
        }
        else
        {
            try
            {
                particle_L.SetActive(false);
            }
            catch { }
        }

        if (_input_R != null)
        {
            try
            {
                particle_R.SetActive(true);
            }
            catch { }
        }
        else
        {
            try
            {
                particle_R.SetActive(false);
            }
            catch { }
        }

        if (_input_D != null)
        {
            try
            {
                particle_D.SetActive(true);
            }
            catch { }
        }
        else
        {
            try
            {
                particle_D.SetActive(false);
            }
            catch{ }
        }

        if (_input_R != null && _input_L != null && !isInteracting)
        {
            isInteracting = true;
            Interact();
        }
    }

    public virtual void DisactiveIcons()
    {
        isInteracting = false;
    }

#if UNITY_EDITOR

    [MenuItem("CONTEXT/ActionCard/Add Input Left")]
    private static void AddInputLeft(MenuCommand menuCommand)
    {
        var actionCard = menuCommand.context as ActionCard;

        foreach (Transform child in actionCard.transform)
        {
            Connector c = child.GetComponent<Connector>();

            if(c != null)
            {
                if (c.side.Equals(ConnectorSide.LEFT))
                    return;
            }
        }

        GameObject o = new GameObject("Input_L", typeof(Connector));
        o.transform.SetParent(actionCard.transform);
        o.GetComponent<Connector>().side = ConnectorSide.LEFT;
        o.GetComponent<Connector>().Init();

    }

    [MenuItem("CONTEXT/ActionCard/Add Input Right")]
    private static void AddInputRight(MenuCommand menuCommand)
    {
        var actionCard = menuCommand.context as ActionCard;

        foreach (Transform child in actionCard.transform)
        {
            Connector c = child.GetComponent<Connector>();

            if (c != null)
            {
                if (c.side.Equals(ConnectorSide.RIGHT))
                    return;
            }
        }

        GameObject o = new GameObject("Input_R", typeof(Connector));
        o.transform.SetParent(actionCard.transform);
        o.GetComponent<Connector>().side = ConnectorSide.RIGHT;
        o.GetComponent<Connector>().Init();
    }

    [MenuItem("CONTEXT/ActionCard/Add InputDown")]
    private static void AddInputDown(MenuCommand menuCommand)
    {
        var actionCard = menuCommand.context as ActionCard;

        foreach (Transform child in actionCard.transform)
        {
            Connector c = child.GetComponent<Connector>();

            if (c != null)
            {
                if (c.side.Equals(ConnectorSide.DOWN))
                    return;
            }
        }

        GameObject o = new GameObject("Input_D", typeof(Connector));
        o.transform.SetParent(actionCard.transform);
        o.GetComponent<Connector>().side = ConnectorSide.DOWN;
        o.GetComponent<Connector>().Init();
    }

#endif

    public virtual void Attach(object[] data)
    {
        if ((ConnectorSide)data[0] == ConnectorSide.LEFT)
        {
            _input_L = (GameObject)data[1];
            isInteracting = false;
            DisactiveIcons();
        }

        if ((ConnectorSide)data[0] == ConnectorSide.RIGHT)
        {
            _input_R = (GameObject)data[1];
            isInteracting = false;
            DisactiveIcons();
        }

        if ((ConnectorSide)data[0] == ConnectorSide.DOWN)
        {
            _input_D = (GameObject)data[1];
            isInteracting = false;
            DisactiveIcons();
        }
    }

    public void Detach(object side)
    {
        if ((ConnectorSide)side == ConnectorSide.LEFT)
        {
            _input_L = null;
            DisactiveIcons();
            isInteracting = false;
            StopAllCoroutines();
        }

        if ((ConnectorSide)side == ConnectorSide.RIGHT)
        {
            _input_R = null;
            DisactiveIcons();
            isInteracting = false;
            StopAllCoroutines();
        }

        if ((ConnectorSide)side == ConnectorSide.DOWN)
        {
            _input_D = null;
            DisactiveIcons();
            isInteracting = false;
            StopAllCoroutines();
        }
    }

    public virtual void TurnOff()
    {
        _input_L = null;
        _input_R = null;
        _input_D = null;
    }

    public virtual void Interact() { }
}
