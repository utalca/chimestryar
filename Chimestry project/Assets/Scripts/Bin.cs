﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bin : ActionCard
{

    protected override void Update()
    {
        base.Update();

        if (_input_R != null && !isInteracting)
        {
            isInteracting = true;
            Interact();
        }
    }

    public override void Interact()
    {
        try
        {
            _input_R.GetComponentInChildren<Instancer>().instance.GetComponentInChildren<Container>().Clear();
        }
        catch
        { }
    }

}