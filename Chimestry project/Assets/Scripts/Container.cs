﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public abstract class Container : Card
{
    public new string name = "[CONTENEDOR]";
    [Range(-1,100)]
    public float capacity;
    public bool isOpen;
    public ContainersTypes type;
    public List<Compound> compound = new List<Compound>();
    public bool havePhenoftalein;

    private Size size = Size.MEDIUM;
    [HideInInspector] public float deltaVolume = 0.1f;
    public float sizeOffset = 1;

    public Compound.StatestateOfMatter[] acceptedStates;

    public float Volume
    {
        get
        {
            if (capacity.Equals(-1))
                return -1;

            float aux = 0;

            foreach (Compound c in compound)
            {
                aux += c.Volume;
            }

            aux = Mathf.Clamp(aux, 0, capacity);

            return aux;
        }
    }

    public float Weight
    {
        get
        {
            float aux = 0;

            foreach (Compound c in compound)
            {
                aux += c.Weight;
            }

            return aux;
        }
    }

    public Units Units
    {
        get
        {
            if (StateOfMatter.Equals(Compound.StatestateOfMatter.LIQUID))
                return Units.ml;

            if (StateOfMatter.Equals(Compound.StatestateOfMatter.GASEOUS))
                return Units.ml;

            return Units.gr;
        }
    }

    public float Temperature
    {
        get
        {
            float aux = 0;
            foreach (Compound c in compound)
            {
                aux += c.celcius;
            }

            aux /= compound.Count;

            return aux;
        }
    }

    public override string ToString()
    {
        if (capacity == -1)
        {
            return GetName() + "\n" + "Infinito";
        }

        if (Units.Equals(Units.gr))
        {
            return GetName() + "\n" + Weight.ToString("F2") + "/" + GetCapacity() + Units.ToString();
        }
        else
        {
            return GetName() + "\n" + Volume.ToString("F2") + "/" + GetCapacity() + Units.ToString();
        }
    }

    public Compound.StatestateOfMatter StateOfMatter
    {
        get
        {
            float solid = 0, liquid = 0, gaseous = 0;

            if (GetCapacity().Equals(-1))
            {
                foreach (Compound c in compound)
                {
                    switch (c.State)
                    {
                        case Compound.StatestateOfMatter.LIQUID:
                            liquid += 1;
                            break;
                        case Compound.StatestateOfMatter.SOLID:
                            solid += 1;
                            break;
                        case Compound.StatestateOfMatter.GASEOUS:
                            gaseous += 1;
                            break;
                    }
                }
            }
            else
            {
                foreach (Compound c in compound)
                {
                    switch (c.State)
                    {
                        case Compound.StatestateOfMatter.LIQUID:
                            liquid += c.Volume;
                            break;
                        case Compound.StatestateOfMatter.SOLID:
                            solid += c.Volume;
                            break;
                        case Compound.StatestateOfMatter.GASEOUS:
                            gaseous += c.Volume;
                            break;
                    }
                }
            }

            if (solid >= liquid && solid >= gaseous)
            {
                return Compound.StatestateOfMatter.SOLID;
            }else if (liquid >= solid && liquid >= gaseous)
            {
                return Compound.StatestateOfMatter.LIQUID;
            }else
            {
                return Compound.StatestateOfMatter.GASEOUS;
            }
        }
    }

    public bool IsFilled
    {
        get
        {
            return Volume == GetCapacity();
        }
    }

    public bool IsEmpty
    {
        get
        {
            return Volume == 0;
        }
    }

    public float Mol
    {
        get
        {
            float aux = 0;

            foreach (Compound c in compound)
            {
                foreach (Element e in c.elements)
                {
                    aux += (e.Weight * e.amount);
                }
            }

            return aux;
        }
    }

    public bool CompatibleMatter(Compound.StatestateOfMatter inputMatter)
    {
        foreach (Compound.StatestateOfMatter sm in acceptedStates)
        {
            if (inputMatter.Equals(sm))
                return true;
        }

        return false;
    }

    private void OnEnable()
    {
        UpdateVolume();
        DisplayVolume();
    }

    void Start()
    {
        transform.GetComponentInChildren<Animator>();
    }

    public bool CanAdd(Compound[] compound)
    {
        float aux = Volume;

        foreach (Compound c in compound)
        {
            aux += c.Volume;
        }

        if (aux > capacity)
        {
            return false;
        }

        return true;
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        GUIStyle style = new GUIStyle();
        style.normal.textColor = Color.blue;
        Handles.Label(transform.position, GetName() + "\n" + ToString(), style);
    }
#endif

    public virtual void Add(Compound[] compound)
    {
        if (this.compound.Count > 0)
        {
            List<Compound> newCompounds = new List<Compound>();

            foreach (Compound c in compound)
            {
                foreach (Compound myCompound in this.compound)
                {
                    if (c.Name.Equals(myCompound.Name))
                    {
                        myCompound.Volume += c.Volume;
                        continue;
                    }

                    newCompounds.Add(Compound.CloneCompound(c));
                }
            }

            this.compound.AddRange(newCompounds);
        }
        else{

            this.compound.AddRange(compound);
        }

        DisplayVolume();
    }

    public virtual Compound[] Substract(float delta)
    {
        if (!(compound.Count > 0))
            return null;

        float proporcionalDelta = delta / compound.Count;

        Compound[] aux = Compound.CloneCompound(compound);

        List<int> toErase = new List<int>();

        for (int i = 0; i < compound.Count; i++)
        {
            float amount = compound[i].Volume - proporcionalDelta;

            if (amount < 0)
            {
                aux[i].Volume = proporcionalDelta - compound[i].Volume;
                toErase.Add(i);
                continue;
            }

            aux[i].Volume = proporcionalDelta;
            compound[i].Volume -= proporcionalDelta;
        }

        foreach (int i in toErase)
        {
            compound.RemoveAt(i);
        }

        DisplayVolume();

        return aux;
    }

    public virtual void DisplayVolume()
    {
        try
        {
            GetComponent<ContentDisplay>().UpdateContent(Volume, GetCapacity(), Compound.GetColor(compound.ToArray()));
        }
        catch { }
    }

    public void AddPheno()
    {
        havePhenoftalein = true;
    }

    protected virtual bool UpdateVolume ()
    {
        if (capacity == -1)
            return false;

        switch (size)
        {
            case Size.SMALLER:
                capacity = 0.1f;
                transform.localScale = Vector3.one / 2 * sizeOffset;
                break;
            case Size.SMALL:
                capacity = 1f;
                transform.localScale = Vector3.one * sizeOffset;
                break;
            case Size.MEDIUM:
                capacity = 10f;
                transform.localScale = Vector3.one * 1.5f * sizeOffset;
                break;
            case Size.BIG:
                capacity = 100f;
                transform.localScale = Vector3.one * 2 * sizeOffset;
                break;
            case Size.BIGGER:
                capacity = 1000f;
                transform.localScale = Vector3.one * 3f * sizeOffset;
                break;
        }

        deltaVolume = capacity / 10;
        return true;
    }

    public override void Interact()
    {
        base.Interact();
        React();
    }

    public void Clear()
    {
        compound.Clear();
    }

    private void React()
    {
        if (Compound.CanReact(compound.ToArray()))
        {
            Compound[] reaction = Compound.GetReaction(compound.ToArray()).ToArray();

            compound.Clear();

            compound.AddRange(reaction);
        }

        RemoveInvalidCompounds();
    }

    private void RemoveInvalidCompounds()
    {
        Compound aux = null;

        foreach (Compound c in compound)
        {
            bool found = false;
            foreach (Compound.StatestateOfMatter validState in acceptedStates)
            {
                if (validState.Equals(c.State))
                {
                    found = true;
                    break;
                }
            }

            if (found == false)
            {
                aux = c;
                break;
            }
        }

        if(aux != null)
        {
            compound.Remove(aux);
            RemoveInvalidCompounds();
        }
    }

    public Compound GetCompound(Compound target)
    {
        foreach (Compound c in compound)
        {
            if (c.Name.Equals(target.Name))
            {
                return c;
            }
        }

        return null;
    }

    public float GetCapacity()
    {
        return capacity;
    }

    public string GetName()
    {
        return name;
    }

    public Size Size
    {
        get
        {
            return size;
        }

        set
        {
            size = value;
            UpdateVolume();
        }
    }

    public virtual void TurnOff()
    {
        instancer.target = null;
    }
}

public enum Size
{
    SMALLER,
    SMALL,
    MEDIUM,
    BIG,
    BIGGER
}

public enum ContainersTypes
{
    FLASK,
    ERLENMEYER,
    DESTILATION,
    PIPPETTE,
    SPOON,
    BALLOON,
    CHEMICAL,
    FLASK_BALLOON,
    TITULATION,
    BUNSEN,
    BUNSEN_STATION
}

public enum Units
{
    ml,
    gr
}


