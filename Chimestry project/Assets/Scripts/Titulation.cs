﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Titulation : Container {

    public GameObject canvas;

    public ContentDisplay pippette_Content;
    public ContentDisplay destilation_Content;
   
    public bool isDropping;

    public float _amount;
    private bool _validReaction;

    float _pippete_capacity;
    float _destilation_capacity;

    Compound[] _pippete_compounds;
    Compound[] _destilation_compounds;

    public override void Init(object[] data)
    {
        Container prev_Input = (Container)data[0];
        Container prev_Output = (Container)data[1];

        if (prev_Input.type.Equals(ContainersTypes.PIPPETTE))
        {
            _pippete_compounds = Compound.CloneCompound(prev_Input.compound);
            _destilation_compounds = Compound.CloneCompound(prev_Output.compound);

            _pippete_capacity = prev_Input.GetCapacity();
            _destilation_capacity = prev_Output.GetCapacity();

            havePhenoftalein = prev_Output.havePhenoftalein;
        }
        else
        {
            _pippete_compounds = Compound.CloneCompound(prev_Output.compound);
            _destilation_compounds = Compound.CloneCompound(prev_Input.compound);

            _pippete_capacity = prev_Output.GetCapacity();
            _destilation_capacity = prev_Input.GetCapacity();

            havePhenoftalein = prev_Input.havePhenoftalein;
        }

        compound = new List<Compound>();

        capacity = prev_Input.capacity + prev_Output.capacity;

        compound.AddRange(prev_Input.compound);
        compound.AddRange(prev_Output.compound);

        _amount = (prev_Input.Volume + prev_Output.Volume) * 2;

        prev_Input.DisplayVolume();
        prev_Output.DisplayVolume();
        DisplayVolume();
    }

    public override void DisplayVolume()
    {
        destilation_Content.UpdateContent(Volume, capacity, Compound.GetColor(_destilation_compounds));
    }

    public override void Interact()
    {
        isDropping = !isDropping;

        if (_validReaction)
            return;

        if (Compound.CompoundExists(_pippete_compounds, RegisteredCompounds.NAOH))
        {
            if (Compound.CompoundExists(_destilation_compounds, RegisteredCompounds.HCL))
            {
                if (havePhenoftalein)
                {
                    _validReaction = true;
                }
            }
        }
    }

    private void Update()
    {
        if (isDropping && _validReaction)
        {
            if (_amount <= 0)
            {
                destilation_Content.LerpColor( Color.magenta, Time.deltaTime);
                canvas.SetActive(true);
            }else
            {
                _amount -= Time.deltaTime;
            }
        }
    }

}
