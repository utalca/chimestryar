﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Connector : MonoBehaviour {

    public ConnectorSide side = ConnectorSide.LEFT;

    private BoxCollider _collider;

    public void Init()
    {
       if(_collider == null)
            _collider = gameObject.AddComponent<BoxCollider>();

       if(side == ConnectorSide.LEFT)
        {
            transform.localPosition = new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z);
        }

        if (side == ConnectorSide.RIGHT)
        {
            transform.localPosition = new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z);
        }

        if (side == ConnectorSide.DOWN)
        {
            transform.localPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z - 0.5f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        object[] data = new object[2];
        data[0] = side;
        data[1] = other.gameObject;

        SendMessageUpwards("Attach", data, SendMessageOptions.DontRequireReceiver);
    }

    private void OnTriggerExit(Collider other)
    {
        object data = (object)side;

        SendMessageUpwards("Detach", data, SendMessageOptions.DontRequireReceiver);
    }
}

public enum ConnectorSide
{
    LEFT,
    RIGHT,
    DOWN
}
