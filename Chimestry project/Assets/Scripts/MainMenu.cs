﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	public void LoadExperiment(int experiment)
    {
        switch (experiment)
        {
            case 0:
                StartCoroutine(LoadSandbox());
                break;
            case 1:
                StartCoroutine(LoadExp(experiment));
                break;
            case 2:
                StartCoroutine(LoadExp(experiment));
                break;
            case 3:
                StartCoroutine(LoadExp(experiment));
                break;
            default:
                StartCoroutine(LoadMainMenu());
                break;

        }
    }

    public void UrlLink()
    {
        Application.OpenURL("www.utalca.cl/");
    }

    IEnumerator LoadSandbox()
    {
        AsyncOperation async = SceneManager.LoadSceneAsync("Exp_Free", LoadSceneMode.Single);
        async.allowSceneActivation = false;
       
        yield return new WaitForSeconds(3);

        async.allowSceneActivation = true;
    }

    IEnumerator LoadMainMenu()
    {
        AsyncOperation async = SceneManager.LoadSceneAsync("Main menu", LoadSceneMode.Single);
        async.allowSceneActivation = false;

        yield return new WaitForSeconds(3);

        async.allowSceneActivation = true;
    }

    IEnumerator LoadExp(int index)
    {
        AsyncOperation async = SceneManager.LoadSceneAsync("Exp_" + index, LoadSceneMode.Single);
        async.allowSceneActivation = false;

        yield return new WaitForSeconds(3);

        async.allowSceneActivation = true;
    }
}
