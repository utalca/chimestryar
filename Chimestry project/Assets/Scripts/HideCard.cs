﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class HideCard : MonoBehaviour, ITrackableEventHandler
{
    private Renderer plane;
    private Collider[] coll;
    private TrackableBehaviour mTrackableBehaviour;

    private Instancer instancer;

    private Instancer Instancer
    {
        get
        {
            if (instancer == null)
                instancer = GetComponent<Instancer>();

            return instancer;
        }
    }

    private void Start()
    {
        plane = transform.Find("Plane").GetComponent<Renderer>();
        coll = GetComponentsInChildren<Collider>();

        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED || newStatus == TrackableBehaviour.Status.TRACKED || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            ToggleVisibility(true);
        }
        else
        {
            ToggleVisibility(false);
        }
    }

    private void ToggleVisibility(bool value)
    {
        transform.position = new Vector3(100,100,100);

        BroadcastMessage("TurnOff");
        plane.enabled = value;

        foreach (Collider c in coll)
        {
            c.enabled = value;
        }

        try
        {
            Instancer.target = null;
        }
        catch { }
    }
}
