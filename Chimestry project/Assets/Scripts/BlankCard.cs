﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlankCard : Instancer {

    public bool IsFilled
    {
        get
        {
            return instance != null;
        }
    }

    public void AssignCard(GameObject prefab)
    {
        Generate(prefab);
    }

    public void AssignCard(GameObject prefab, object[] data)
    {
        Card c = Generate(prefab);

        try
        {
            c.Init(data);
        }
        catch { }
    }
}
