﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class Instancer : MonoBehaviour, ITrackableEventHandler
{
    public GameObject prefab;
   [HideInInspector] public Transform target;
    [HideInInspector] public GameObject instance;

    private ImageTargetBehaviour targetimage;
    private TrackableBehaviour mTrackableBehaviour;

    private bool isCreated;
    private bool isVisible;

    void Start()
    {
        targetimage = GetComponent<ImageTargetBehaviour>();

        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }

    void LateUpdate()
    {
        if (instance != null && isVisible)
        {
            if (target != null)
            {
                instance.transform.position = Vector3.Lerp(instance.transform.position, target.transform.position, Time.deltaTime * 8);
                instance.transform.rotation = Quaternion.Lerp(instance.transform.rotation, target.transform.rotation, Time.deltaTime * 8);
            }
            else
            {
                instance.transform.position = Vector3.Lerp(instance.transform.position, targetimage.transform.position, Time.deltaTime * 8);
                instance.transform.rotation = Quaternion.Lerp(instance.transform.rotation, targetimage.transform.rotation, Time.deltaTime * 8);
            }
        }
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED || newStatus == TrackableBehaviour.Status.TRACKED || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            if (!isCreated)
            {
                Generate();
            }

            StartCoroutine(ToggleVisibility(true));
        }
        else
        {
            StartCoroutine(ToggleVisibility(false));
        }
    }

    private void Generate()
    {
        if (prefab == null)
            return;

        instance = Instantiate(prefab, targetimage.transform.position, targetimage.transform.rotation);
        instance.name += "_Instance";
        instance.GetComponent<Card>().Set(this);
        isCreated = true;
    }

    public Card Generate(GameObject prefab)
    {
        if (isCreated)
            return null;

        this.prefab = prefab;

        instance = Instantiate(prefab, targetimage.transform.position, targetimage.transform.rotation);
        instance.name += "_Instance";
        Card c = instance.GetComponent<Card>();
        c.Set(this);
        isCreated = true;
        isVisible = true;
        SetTarget(null);

        return c;
    }

    public virtual void TurnOff() { }

    private IEnumerator ToggleVisibility(bool value)
    {
        if (instance == null)
        {
            yield break;
        }

        isVisible = value;

        if (isVisible)
        {
            instance.SetActive(isVisible);
            StopAllCoroutines();
            yield break;
        }

        yield return new WaitForSeconds(1);

        instance.SetActive(isVisible);
    }
}
