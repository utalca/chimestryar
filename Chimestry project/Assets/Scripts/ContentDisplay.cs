﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContentDisplay : MonoBehaviour {

    public GameObject[] states;

    public void UpdateContent(float volume, float capacity, Color color)
    {
        if (volume.Equals(0))
        {
            DisactiveAllStates();
            return;
        }

        int index = (int)Mathf.Round((volume * states.Length) / capacity);

        index = Mathf.Clamp(index, 0, states.Length-1);

        DisactiveAllStates();

        states[index].SetActive(true);
        states[index].GetComponent<Renderer>().material.color = color;
    }

    public void LerpColor(Color newColor, float t)
    {
        foreach (GameObject go in states)
        {
            go.GetComponent<Renderer>().material.color = Color.Lerp(go.GetComponent<Renderer>().material.color, newColor, t);
        }
    }

    private void DisactiveAllStates()
    {
        foreach (GameObject go in states)
        {
            go.SetActive(false);
        }
    }

    public void AddSlot()
    {
        if (states == null)
            states = new GameObject[1];

        GameObject[] aux = states;

        states = new GameObject[aux.Length + 1];

        int i = 0;
        foreach (GameObject go in aux)
        {
            states[i] = go;
            i++;
        }
    }

    public void RemoveSlot()
    {
        if (!(states.Length > 0))
            return;

        GameObject[] aux = states;

        states = new GameObject[aux.Length - 1];

        int i = 0;
        foreach (GameObject go in states)
        {
            states[i] = aux[i];
            i++;
        }
    }

}
