﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoCard : ActionCard
{
    public Text text;

    [System.NonSerialized] public Card card;

    public virtual void DisplayInfo()
    {
    }

    public override void TurnOff()
    {
        base.TurnOff();
        Cancel();
    }

    public virtual void DisactiveStatus()
    {
        Color g = text.color;
        g.a = 0;
        text.color = g;
    }

    public virtual void ActiveStatus()
    {
        text.color = Color.green;
    }

    private void Cancel()
    {
        DisactiveStatus();
        card = null;
    }
}
