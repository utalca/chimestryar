﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationControllerContainer : StateMachineBehaviour {

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (stateInfo.IsName("Closed"))
        {
            animator.SetBool("IsOpen", animator.GetComponentInParent<Container>().isOpen);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        if (stateInfo.IsName("Open"))
        {
            animator.GetComponentInParent<Container>().isOpen = true;
            animator.SetBool("IsOpen", animator.GetComponentInParent<Container>().isOpen);
        }

        if (stateInfo.IsName("Close"))
        {
            animator.GetComponentInParent<Container>().isOpen = false;
            animator.SetBool("IsOpen", animator.GetComponentInParent<Container>().isOpen);
        }
    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
