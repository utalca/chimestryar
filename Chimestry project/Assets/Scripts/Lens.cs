﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lens : InfoCard {

    public override void DisplayInfo()
    {
        try
        {
            Container[] c = card.GetComponentsInChildren<Container>();

            text.text = string.Empty;

            float aux = 0;

            foreach (Container con in c)
            {
                text.text = "-> ";

                foreach (Compound com in con.compound)
                {
                    text.text += com.Name + " + ";
                }

                text.text = text.text.Substring(0, text.text.Length - 3);
                text.text = "\n";
            }

            text.text = "\n";

            foreach (Container con in c)
            {
                aux += con.Mol;

                foreach (Compound com in con.compound)
                {
                    text.text += com.Name + ": " + com.MolecularWeight + " gr \n";
                }
            }

            text.text += "Total: " + aux + " gr";
        }
        catch
        {
            text.text = "No es posible analizar este objeto.";
            text.color = Color.red;
        }
    }

}
