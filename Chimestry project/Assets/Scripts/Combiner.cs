﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Combiner : ActionCard {

    enum ValidationExceptions
    {
        INVALID_RECIPE,
        INVALID_TOOL,
        INPUT_CLOSED,
        OUTPUT_CLOSED,
        INPUT_EMPTY,
        OUTPUT_FILLED,
        SUCCESS
    }

    public Text error_t, input_t, output_t;
    public Image arrow_i;

    public GameObject[] errorIcons;
    public GameObject[] actionIcons;

    private Container _container_Left;
    private Container _container_Right;

    public override void TurnOff()
    {
        base.TurnOff();
        Cancel();
    }

    public override void Interact()
    {
        try
        {
            _container_Left = _input_L.GetComponent<Instancer>().instance.GetComponent<Container>();
        }
        catch
        {
            print("Container left not found in: " + _input_L.name);
        }

        try
        {
            _container_Right = _input_R.GetComponent<Instancer>().instance.GetComponent<Container>();
        }
        catch
        {
            print("Container right not found");
        }


        if (_container_Left == null || _container_Right == null)
        {
            DisactiveStatus();
            isInteracting = false;
            ShowErrorIcon((int)ValidationExceptions.INVALID_RECIPE);
            return;
        }

        switch (ValidateRecipe(_container_Left, _container_Right))
        {
            case ValidationExceptions.SUCCESS:
                StartCoroutine(Combine(_container_Left, _container_Right));
                return;
            case ValidationExceptions.INVALID_RECIPE:
                ShowErrorIcon((int)ValidationExceptions.INVALID_RECIPE);
                break;
            case ValidationExceptions.INVALID_TOOL:
                ShowErrorIcon((int)ValidationExceptions.INVALID_TOOL);
                break;
            case ValidationExceptions.INPUT_CLOSED:
                ShowErrorIcon((int)ValidationExceptions.INPUT_CLOSED);
                break;
            case ValidationExceptions.OUTPUT_CLOSED:
                ShowErrorIcon((int)ValidationExceptions.OUTPUT_CLOSED);
                break;
            case ValidationExceptions.INPUT_EMPTY:
                ShowErrorIcon((int)ValidationExceptions.INPUT_EMPTY);
                break;
            case ValidationExceptions.OUTPUT_FILLED:
                ShowErrorIcon((int)ValidationExceptions.OUTPUT_FILLED);
                break;
        }

        DisactiveStatus();
        isInteracting = false;
    }

    private void ShowErrorIcon(int i)
    {
        errorIcons[i].SetActive(true);
    }

    private void ShowActionIcon(int i)
    {
        actionIcons[i].SetActive(true);
    }

    public override void DisactiveIcons()
    {
        foreach (GameObject go in errorIcons)
        {
            go.SetActive(false);
        }

        foreach (GameObject go in actionIcons)
        {
            go.SetActive(false);
        }
    }

    private void Cancel()
    {
        _container_Left = null;
        _container_Right = null;
        DisactiveStatus();
    }

    private void DisactiveStatus()
    {
        Color g = arrow_i.color;
        g.a = 0;
        arrow_i.color = g;
        input_t.color = g;
        output_t.color = g;
    }

    private void ActiveStatus()
    {
        Color g = arrow_i.color;
        g.a = 1;
        arrow_i.color = g;
        input_t.color = g;
        output_t.color = g;
    }

    private IEnumerator Combine(Container input, Container output)
    {
        if (output.IsFilled)
            DisactiveIcons();

        if (input.IsEmpty)
            DisactiveIcons();

        input_t.text = input.ToString();
        output_t.text = output.ToString();

        switch (PourRecipes.Singleton.GetResult(_container_Left.type, _container_Right.type))
        {
            case PourResults.DEFAULT:
                ShowActionIcon((int)PourResults.DEFAULT);
                break;
            case PourResults.SPOON_DRAIN:
                ShowActionIcon((int)PourResults.SPOON_DRAIN);
                break;
            case PourResults.SPOON_FILL:
                ShowActionIcon((int)PourResults.SPOON_FILL);
                break;
            case PourResults.PIPPETTE_DRAIN:
                ShowActionIcon((int)PourResults.PIPPETTE_DRAIN);
                break;
            case PourResults.PIPPETTE_FILL:
                ShowActionIcon((int)PourResults.PIPPETTE_FILL);
                break;
        }

        yield return new WaitForSeconds(2);

        ActiveStatus();

        if (_input_L != null && _input_R != null)
        {
            Compound[] original = Compound.CloneCompound(input.compound);
            Compound[] substracted = input.Substract(1);

            if (output.CanAdd(substracted))
            {
                float aux = 0;

                foreach (Compound c in substracted)
                {
                    aux += c.Volume;
                }

                output.Add(substracted);
            }
            else
            {
                input.compound = new List<Compound>(original);
            }

            input_t.text = input.ToString();
            output_t.text = output.ToString();
        }

        yield return new WaitForSeconds(2);

        isInteracting = false;
    }

    private ValidationExceptions ValidateRecipe(Container input, Container output)
    {
        if (!output.CompatibleMatter(input.StateOfMatter))
        {
            return ValidationExceptions.INVALID_TOOL;
        }

        if (!input.isOpen)
        {
            return ValidationExceptions.INPUT_CLOSED;
        }

        if (!output.isOpen)
        {
            return ValidationExceptions.OUTPUT_CLOSED;
        }

        if (input.Volume.Equals(0))
        {
            return ValidationExceptions.INPUT_EMPTY;
        }

        if (output.IsFilled)
        {
            return ValidationExceptions.OUTPUT_FILLED;
        }

        if (PourRecipes.Singleton.GetResult(input.type, output.type).Equals(PourResults.NONE))
        {
            return ValidationExceptions.INVALID_RECIPE;
        }

        return ValidationExceptions.SUCCESS;
    }
}
