﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public abstract class Card : MonoBehaviour {

    [HideInInspector] public Instancer instancer;

    public void Set(Instancer instancer)
    {
        this.instancer = instancer;
    }

    public virtual void Init(object[] data)
    {

    }

    public virtual void Interact()
    {
    }
}
