﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Thermometer : InfoCard {

    public float maxHeatValue = 100;

    public Image bar_background;
    public Image bar_amount;
    public Text percentage;

    public override void DisplayInfo()
    {
        try
        {
            Container[] c = card.GetComponentsInChildren<Container>();
            float aux = 0;
            int i = 0;
            text.text = string.Empty;

            foreach (Container con in c)
            {
                foreach (Compound com in con.compound)
                {
                    aux += com.celcius;
                    text.text +=  "\n" + com.Name + " : " + com.celcius + " C°";
                    i++;
                }
            }

            aux /= i;

            StartCoroutine(UpdateBar(aux, maxHeatValue));
        }
        catch
        {
            DisactiveStatus();
            text.text = "No es posible medir la temperatura de este objeto.";
            text.color = Color.red;
        }
    }

    private IEnumerator UpdateBar(float current, float limit)
    {
        float aux = 0.1f;

        float final = current / limit;

        do
        {
            percentage.text = (aux * limit).ToString("F2") + " C°";

            bar_amount.fillAmount = aux;

            aux += (((aux / limit)) / 1) * Time.deltaTime * 50;

            yield return new WaitForSeconds(Time.deltaTime);
        }
        while (aux < final);

        percentage.text = (final * limit).ToString("F2") + " C°";
        bar_amount.fillAmount = final;
    }

    public override void TurnOff()
    {
        base.TurnOff();
        DisactiveStatus();
    }

    public override void DisactiveStatus()
    {
        Color info = text.color;
        info.a = 0;
        text.color = info;
        Color perc = percentage.color;
        perc.a = 0;
        percentage.color = perc;
        Color bar_back = bar_background.color;
        bar_back.a = 0;
        bar_background.color = bar_back;
        Color bar_value = bar_amount.color;
        bar_value.a = 0;
        bar_amount.color = bar_value;
    }

    public override void ActiveStatus()
    {
        Color info = text.color;
        info.a = 1;
        text.color = info;
        Color perc = percentage.color;
        perc.a = 1;
        percentage.color = perc;
        Color bar_back = bar_background.color;
        bar_back.a = 1;
        bar_background.color = bar_back;
        Color bar_value = bar_amount.color;
        bar_value.a = 1;
        bar_amount.color = bar_value;
    }
}
